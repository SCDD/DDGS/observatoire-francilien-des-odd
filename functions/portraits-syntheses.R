
#' Permet d'obtenir une statistique sur un indicateur
#'
#' @param indicateur Un code indicateur.
#' @param fun Une fonction retournant une statistique.
#' @param echelle_geo Echelle pour calculer la statistique (com ou epci)
#'
#' @return La statistique calculée selon la fonction fournise.
#' @export
#'
#' @examples
#' obtenir_stat_indicateur("pauv17", max)
#' obtenir_stat_indicateur("pauv17", min)
obtenir_stat_indicateur <- function(indicateur, fun, echelle_geo = c("com", "epci", "reg")) {
  fun <- match.fun(fun)
  echelle_geo <- match.arg(echelle_geo)
  bdd_odd %>%
    filter(libvar == indicateur, echelle == echelle_geo) %>%
    pull(valeur) %>%
    as.numeric() %>%
    fun(na.rm = TRUE)
}




#' Preparation des données pour affichage dansle tableau de synthèse
#'
#' @param code_geo Un code INSEE ou un code EPCI
#'
#' @return un tableau de données
#' @export
#'
#' @examples
#' preparer_donnees_synthese("77193")
#' preparer_donnees_synthese("200072130")
#'
preparer_donnees_synthese <- function(code_geo) {

  base <- bdd_odd %>%
    filter(codgeo == code_geo)

  echelle <- base$echelle[1]

  resultat <- base %>%
    mutate(across(starts_with("valeur"), as.numeric)) %>%
    arrange(odd_id, libodd, libelle, libgeo, codgeo, annee) %>%
    group_by(odd_id, libodd, libelle, libgeo, codgeo) %>%
    slice_max(annee) %>%
    filter(valeur >= 0) %>%
    arrange(nchar(odd_id), odd_id, libvar) %>%
    ungroup() %>%
    mutate(
      comparaison_epci = valeur - valeur_epci,
      comparaison_region = valeur - valeur_region
    ) %>%
    group_by(libvar) %>%
    mutate(
      comparaison_epci_picto = trouver_comparaison_picto(libvar, comparaison_epci, "epci"),
      comparaison_region_picto = trouver_comparaison_picto(libvar, comparaison_region, "region")
    ) %>%
    mutate(odd_id2 = odd_id, libvar2 = libvar, libelle2 = libelle) %>%
    ungroup() %>%
    nest(
      infos_odd = c(odd_id, libelle, annee, libvar),
      valeur_commune = c(libvar, valeur, odd_id, libelle),
      comparaison_epci = c(comparaison_epci, comparaison_epci_picto, libvar),
      comparaison_region = c(comparaison_region, comparaison_region_picto, libvar),
      .by = libvar2
    ) %>%
    select(infos_odd, valeur_commune, comparaison_epci, comparaison_region)

  if (isTRUE(echelle %in% c("epci", "mgp"))) {
    resultat$comparaison_epci <- NULL
  }

  return(resultat)
}


trouver_comparaison_picto <- function(indicateur, valeur = NULL, niveau = c("epci", "region")) {
  if (length(indicateur) < 1)
    return(-999)
  if (!est_comparable(indicateur))
    return(-999)
  niveau <- match.arg(niveau)
  donnees <- bdd_odd %>%
    filter(libvar == indicateur) %>%
    mutate(across(starts_with("valeur"), as.numeric))
  if (niveau == "epci") {
    valeurs <- donnees %>%
      mutate(comparaison = valeur - valeur_epci)
  } else {
    valeurs <- donnees %>%
      mutate(comparaison = valeur - valeur_region)
  }
  valeurs <- pull(valeurs, comparaison)
  mediane_pos <- median(valeurs[valeurs > 0], na.rm = TRUE)
  if (is.na(mediane_pos))
    mediane_pos <- 1
  mediane_neg <- median(valeurs[valeurs < 0], na.rm = TRUE)
  if (is.na(mediane_neg))
    mediane_neg <- -1
  if (is.null(valeur))
    valeur <- valeurs
  if (all(is.na(valeur)))
    return(valeur)
  findInterval(valeur, c(-Inf, mediane_neg, -0.01, 0.01, mediane_pos, Inf)) - 3
}


creer_tableau_synthese <- function(donnees, clickPopupId = "clickPopupId") {
  avec_epci <- hasName(donnees, "comparaison_epci")
  reactable(
    data = donnees,
    pagination = FALSE,
    compact = TRUE,
    sortable = FALSE,
    class = "tableau-odd",
    theme = reactableTheme(
      cellStyle = list(display = "flex", flexDirection = "column", justifyContent = "center")
    ),
    columns = dropNulls(list(
      infos_odd = col_def_odd(clickPopupId),
      valeur_commune = col_def_valeur(avec_epci),
      comparaison_epci = if (avec_epci) col_def_comparaison("Écart avec l'EPCI"),
      comparaison_region = if (!avec_epci) col_def_comparaison("Écart avec la région") else
        col_def_comparaison("Écart avec la région")
    )),
    language = reactableLang(noData = "Pas de données à afficher")
  )
}

col_def_odd <- function(clickPopupId) {
  colDef(
    name = "Objectifs de développement durable",
    minWidth = 300,
    html = TRUE,
    cell = function(value) {
      cet_odd <- odd_infos() %>%
        filter(odd_id2 == value$odd_id[1])
      tagList(
        tags$div(
          style = "white-space: nowrap; overflow: hidden; text-overflow: ellipsis;",
          tags$img(
            src = sprintf("images/logo-sdg-inverted-croped/SDG_Icons_Inverted_Transparent_PRINT-%s.png", cet_odd$odd_num),
            style = "height: 30px;"
          ),
          tags$b(
            cet_odd$odd_libelle,
            title = cet_odd$odd_libelle,
            style = css(marginLeft = "10px", color = cet_odd$odd_couleur)
          )
        ),
        # tags$br(),
        tags$div(
          value$libelle, paste0(" (", value$annee, ")"),
          bouton_fiche_indicateur(clickPopupId, value$odd_id, value$libvar)
        )
      ) %>%
        htmltools::doRenderTags()
    }
  )
}


col_def_valeur <- function(avec_epci) {
  colDef(
    name = if (avec_epci) "Valeur pour la commune" else "Valeur pour l'EPCI",
    minWidth = 250,
    html = TRUE,
    cell = function(value) {
      cet_odd <- odd_infos() %>%
        filter(odd_id2 == value$odd_id)
      unite_de_mesure <- trouver_indicateur_unite(value$libvar)
      valeur <- as.numeric(value$valeur)
      stat_indicateur <- obtenir_stat_indicateur(value$libvar, range, echelle_geo = if (avec_epci) "com" else "epci")
      width <- scales::rescale(
        x = valeur,
        to = c(0, 100),
        from = stat_indicateur
      )
      width <- paste0(width, "%")
      if (valeur == 1) {
        valeur <- format(round(valeur, 1), digits = 3, nsmall = 0)
        valeur_min <- format(round(stat_indicateur[1], 1), digits = 3, nsmall = 0)
        valeur_max <- format(round(stat_indicateur[2], 1), digits = 3, nsmall = 0)
      } else {
        valeur <- format(round(valeur, 1), nsmall = 0)
        valeur_min <- format(round(stat_indicateur[1], 1), nsmall = 0)
        valeur_max <- format(round(stat_indicateur[2], 1), nsmall = 0)
      }
      note_de_lecture <- trouver_note_de_lecture(value$libvar, saut_de_lignes = FALSE)
      note_de_lecture <- glue::glue(
        note_de_lecture,
        valeur = valeur,
        commune = "",
        annee = paste0("20", substring(value$libvar, nchar(value$libvar) - 1))
      )
      tag <- tags$div(
        class = "tableau-odd-bar-cell",
        tags$div(
          valeur, " ", unite_de_mesure,
          popover(
            trigger =  tags$a(
              tabindex = "0",
              ph(
                "question",
                fill = config::get("couleur_principale"),
                title = NULL,
                style = "cursor: pointer;"
              )
            ),
            title = "Note de lecture",
            note_de_lecture,
            options = list(trigger = "focus")
          )
        ),
        tags$div(
          class = "tableau-odd-bar-chart",
          style = "background-color: #e1e1e1",
          title = sprintf("Minimum : %s - Maximum : %s", valeur_min, valeur_max),
          tags$div(
            class = "tableau-odd-bar",
            style = css(width = width, backgroundColor = "#a3a3a3"),
            #tags$div (
            #  class = "line",
            #  style = "background-color: transparent",
            #  title = sprintf(valeur)
            #)
          )
        )
      )
      htmltools::doRenderTags(tag)
    }
  )
}

col_def_comparaison <- function(name) {
  colDef(
    # name = name,
    header = function(value) {
      htmltools::doRenderTags(
        tagList(
          name,
          # if (name == "Écart avec l'EPCI") {
          #   popover(
          #     trigger =  tags$a(
          #       tabindex = "0",
          #         ph(
          #         "question",
          #         fill = config::get("couleur_principale"),
          #         title = NULL,
          #         style = "cursor: pointer;"
          #       )
          #     ),
          #     title = "Note de lecture",
          #     synthese_note_lecture(),
          #     options = list(trigger = "focus")
          #   )
          # } else if (name == "Écart avec la Région") {
          #   popover(
          #     trigger = tags$a(
          #       tabindex = "0",
          #       ph(
          #         "question",
          #         fill = config::get("couleur_principale"),
          #         title = NULL,
          #         style = "cursor: pointer;"
          #       )
          #     ),
          #     title = "Note de lecture",
          #     synthese_note_lecture_epci(),
          #     options = list(trigger = "focus")
          #   )
          # } else {
          #   popover(
          #     trigger =  tags$a(
          #       tabindex = "0",
          #         ph(
          #         "question",
          #         fill = config::get("couleur_principale"),
          #         title = NULL,
          #         style = "cursor: pointer;"
          #       )
          #     ),
          #     title = "Note de lecture",
          #     synthese_note_lecture_region(),
          #     options = list(trigger = "focus")
          #   )
          # }
        )
      )
    },
    html = TRUE,
    align = "center",
    cell = function(value) {
      picto <- value[[2]]
      valeur <- value[[1]]
      valeur <- round(valeur, digits = 2)
      if (is.na(picto)) {
        #return("Pas de données")
         tag <- ph("x", height = 30, fill = "#999", title = "Pas de données")
        return(htmltools::doRenderTags(tag))
      } else if (picto == -2) {
        return(valeur)
    } else if (picto == -1) {
      return(valeur)
    }  else if (picto == 0) {
      tag <-ph("equals", height = 30, fill = "#999", title = valeur)
      return(htmltools::doRenderTags(tag))
    } else if (picto == 1) {
      return(paste("+", valeur))
    } else if (picto == 2) {
      return(paste("+", valeur))
    } else {
      tag<- ph("minus", height = 30, rotate = 135, fill = "#999", title = "Non comparable")
      return(htmltools::doRenderTags(tag))
    }
      # smiley <- trouver_smiley_comparaison(value$libvar)
      # couleurs <- trouver_couleurs_comparaison(value$libvar)
      # tag <- if (picto == -2) {
      #   tagList(
      #     ph(smiley[1], height = 40, fill = couleurs[1], title = valeur)
      #   )
      # } else if (picto == -1) {
      #   ph(smiley[1], height = 40, fill = couleurs[1], title = valeur)
      # } else if (picto == 0) {
      #   ph("equals", height = 40, fill = "#999", title = valeur)
      # } else if (picto == 1) {
      #   valeur <- paste0("+", valeur)
      #   ph(smiley[2], height = 40, fill = couleurs[2], title = valeur)
      # } else if (picto == 2) {
      #   valeur <- paste0("+", valeur)
      #   tagList(
      #     ph(smiley[2], height = 40, fill = couleurs[2], title = valeur)
      #   )
      # } else {
      #   ph("minus", height = 40, rotate = 135, fill = "#999", title = "Non comparable")
      # }
      # htmltools::doRenderTags(tag)
    }
  )
}




#' Note de lecture tableau de synthèse
#'
#' @return Une luste de tag HTML
#' @export
#'
#' @examples
#' htmltools::browsable(synthese_note_lecture())
#'
# synthese_note_lecture <- function() {
#   includeMarkdown(path = "documents/tableau-synthese-note.md")
# }
# 
# synthese_note_lecture_region <- function() {
#   includeMarkdown(path = "documents/tableau-synthese-note-region.md")
# }
# 
# synthese_note_lecture_epci <- function() {
#   includeMarkdown(path = "documents/tableau-synthese-note-epci.md")
# }
