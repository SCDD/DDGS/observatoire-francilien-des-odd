
#' Initialisation carte Leaflet
#'
#' @return un objet `leaflet`
#' @export
#'
#' @examples
#' carto_init() %>%
#'  setView(lng = 2.3522219, lat = 48.856614, zoom = 9)
carto_init <- function() {
  leaflet(options = leafletOptions(preferCanvas = TRUE)) %>%
    addTiles() |> 
    # ajouter_tuiles_ign_orthophotos() %>%
    # ajouter_tuiles_ign_planv2() %>%
    ajouter_zoom(position = "topright") #%>%
    # addLayersControl(
    #   baseGroups = c("PLAN IGN V2", "ORTHOPHOTOS IGN"),
    #   position = "topright"
    #)
}

carto_proxy <- function(carteId) {
  leafletProxy(mapId = carteId) %>%
    clearControls() %>%
    clearShapes() %>%
    clearMarkers()
}


#' Représenter un indicateur sur une carte
#'
#' @param carte Un objet `leaflet`, construit avec [carto_init()] par exemple.
#' @param indicateur Code de l'indicateur à représenter.
#' @param echelle Echelle géographique: communes ou epci.
#'
#' @return Un objet `leaflet`
#' @export
#'
#' @examples
#' carto_init() %>%
#'   carto_ajouter_indicateur("pauv17")
#'
#' carto_init() %>%
#'   carto_ajouter_indicateur("pauv17", echelle = "epci")
carto_ajouter_indicateur <- function(carte,
                                     indicateur,
                                     echelle = c("commune avec arrondissement", "commune avec paris", "commune", "epci", "epci_mgp"),
                                     clickPopupId = NULL) {
  echelle <- match.arg(echelle)
  if (identical(echelle, "commune")) {
    echelle <- trouver_echelle(indicateur, "commune")
  }

  niv_echelle <- switch(
    echelle,
    "commune avec arrondissement" = "arrondissement",
    "commune avec paris" = "commune",
    "epci" = "epci",
    "epci_mgp" = "epci_mgp"
  )

  donnees <- base_geo %>%
    filter(niveau == niv_echelle) %>%
    left_join(
      y = bdd_odd %>%
        filter(libvar == indicateur) %>%
        select(codgeo, valeur),
      by = c("code_geo" = "codgeo")
    )


  suppressWarnings(valeurs <- as.numeric(donnees$valeur))
  type <- trouver_carte_type(indicateur)

  note_de_lecture <- trouver_note_de_lecture(indicateur)
  if (is.na(note_de_lecture)) {
    labels <- sprintf(
      "<b>%s</b><br/>%g %s",
      donnees$libelle,
      valeurs,
      trouver_indicateur_unite(indicateur)
    )
  } else {
    labels <- glue::glue(
      paste0("<b>{commune}</b><br/>", note_de_lecture),
      valeur = valeurs,
      commune = donnees$libelle,
      annee = paste0("20", substring(indicateur, nchar(indicateur) - 1))
    )
  }

  labels[is.na(valeurs)] <- sprintf(
    "<b>%s</b><br/><em>Pas de données</em>",
    donnees$libelle[is.na(valeurs)]
  )
  labels <- lapply(labels, htmltools::HTML)


  if (identical(type, "polygones")) {

    couleur <- trouver_carte_couleur(indicateur, echelle)
    pal <- colorBin(
      palette = couleur$palette,
      domain = valeurs,
      bins = trouver_carte_decoupage(indicateur, echelle, valeurs),
      reverse = couleur$pal_reverse
    )

    carte <- carte %>%
      addPolygons(
        data = donnees,
        color = "white",
        fillColor = pal(valeurs),
        smoothFactor = 1,
        dashArray = "3",
        weight = 1,
        opacity = 0.7,
        fillOpacity = 0.7,
        highlightOptions = highlightOptions(
          color = "white",
          weight = 3,
          fillOpacity = 0.5,
          bringToFront = FALSE,
          dashArray = "",
          opacity = 1
        ),
        label = labels,
        labelOptions = labelOptions(
          style = list("font-weight" = "normal", padding = "4px 8px"),
          textsize = "15px",
          direction = "auto"
        )
      ) %>%
      addPolylines(
        data = filter(base_geo, niveau == "departement"),
        weight = 1.5,
        smoothFactor = 1,
        opacity = 1,
        color = "#595859",
        dashArray = "3",
        fillOpacity = 1
      ) %>%
      addLegend(
        pal = pal,
        values = as.numeric(valeurs),
        na.label = "-",
        opacity = 0.7,
        title = trouver_indicateur_unite(indicateur, type = "libelle") %>%
          strwrap(width = 40) %>%
          paste(collapse = "<br>"),
        position = "topright"
      )
  } else {
    couleur <- trouver_carte_couleur(indicateur)$palette
    taille <- trouver_carte_taille(indicateur, echelle)

    carte <- carte %>%
      addPolygons(
        data = donnees,
        weight = 0.5,
        smoothFactor = 1,
        opacity = 0.8,
        fillColor = "white",
        color = "black" ,
        dashArray = "3",
        fillOpacity = 0.5
      ) %>%
      addPolylines(
        data = filter(base_geo, niveau == "departement"),
        weight = 1.5,
        smoothFactor = 1,
        opacity = 1,
        color = "black",
        dashArray = "3",
        fillOpacity = 1
      ) %>%
      addCircleMarkers(
        data = donnees,
        lng =  ~ x,
        lat =  ~ y,
        weight = 1,
        opacity = 1,
        radius = ~ sqrt(as.numeric(valeurs) / pi) * as.numeric(taille),
        fillOpacity = 0.7,
        fill = TRUE,
        stroke = TRUE,
        color = "white",
        fillColor = couleur
      ) %>%
      addPolygons(
        data = donnees,
        color = "white",
        dashArray = "3",
        weight = 0.1,
        opacity = 0,
        fillOpacity = 0,
        highlightOptions = highlightOptions(
          color = "#666",
          weight = 3,
          fillOpacity = 0,
          bringToFront = TRUE,
          dashArray = "",
          opacity = 1
        ),
        label = labels,
        labelOptions = labelOptions(
          style = list("font-weight" = "normal", padding = "3px 8px"),
          textsize = "12px",
          direction = "auto"
        )
      )
  }
  carte <- carto_ajouter_titre(carte, indicateur, clickPopupId = clickPopupId)
  return(carte)
}


carto_ajouter_titre <- function(carte, indicateur, clickPopupId = "clickPopupId") {
  infos <- bdd_odd_niveau %>%
    filter(libvar == indicateur)
  ODD_ID <- gsub(pattern = "(ODD\\d{1,2}).*", replacement = "\\1", x = infos$indic[1])
  cet_odd <- odd_infos() %>%
    filter(odd_id2 == ODD_ID)
  ajouter_titre(
    carte = carte,
    texte = tags$div(
      style = css(display = "grid", gridTemplateColumns = "70px 1fr", gridColumnGap = "15px"),
      tags$div(
        tags$img(
          src = sprintf("images/logo-odd/F-WEB-Goal-%s.png", cet_odd$odd_num),
          style = "height: 70px;"
        )
      ),
      tags$div(
        tags$div(
          infos$libelle[1],
          if (!is.null(clickPopupId))
            bouton_fiche_indicateur(clickPopupId, ODD_ID, indicateur),
          style = css(fontWeight = "bold", fontSize = "large", color = cet_odd$odd_couleur)
        ),
        tags$p("données", infos$annee[1], style = css(fontSize = "smaller"))
      )
    ),
    position = "topleft"
  )
}

