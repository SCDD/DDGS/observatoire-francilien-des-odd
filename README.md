# Observatoire ODD

> Application de l’observatoire francilien des objectifs du développement durable

Lien vers l'application en ligne : https://ssm-ecologie.shinyapps.io/observatoire-odd/


### Lancer l'application

```r
shiny::runApp("app.R")
```


### Paquets R utilisés

La liste des packages utilisés est consignées dans le fichier `renv.lock`.

Pour installer les packages nécessaires :

```r
renv::restore()
```

Pour mettre à jour les packages :

```r
renv::update()
```

Pour mettre à jour le fichier listant les packages utilisés :

```r
renv::snapshot()
```


### Paramètres acceptés dans l'URL

**Code géo:** Code Insee d'une commune ou code EPCI sélectionnée par défaut dans la page "Portraits de territoire":
```
<URL>/portraits-de-territoire?codgeo=77001
<URL>/portraits-de-territoire?codgeo=200070779
```

**Vue:** vue sélectionnée par défaut dans la page "Portraits de territoire":
```
<URL>/portraits-de-territoire?vue=details
<URL>/portraits-de-territoire?vue=comparaison
```

