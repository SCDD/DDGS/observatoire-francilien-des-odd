## À propos

Les objectifs de développement durable sont une responsabilité partagée par l’ensemble des acteurs et des citoyens des pays signataires de l’Agenda 2030 adopté en 2015. Les 17 ODD et leurs 169 cibles constituent un cadre de référence commun qui doivent désormais être déclinés à toutes les échelles du territoire. 

L’enjeu aujourd’hui est de faciliter l’appropriation de ces objectifs de développement durable pour les décliner concrètement, en mobilisant l’ensemble des acteurs locaux. Le projet d’Observatoire francilien des ODD, porté par la DRIEAT, s’est appuyé sur une gouvernance partenariale via un comité de pilotage composé des représentants de services de l’État, de collectivités et d’associations. 
 
L’Observatoire francilien des ODD a pour objectif de faciliter la mise en œuvre de stratégies territoriales durables en s’appuyant sur des données objectives pour réaliser un état des lieux, mesurer dans le temps l’évolution des indicateurs, et favoriser l’émulation par la comparaison entre territoires. Il est le fruit d’une collaboration avec des producteurs de données publiques (voir ci-dessous). Il a vocation à s’enrichir au fil du temps et pourra être le support d’événements de sensibilisation et de valorisation des ODD.


### Institutions représentées au sein du comité de pilotage :

[DRIEAT](https://www.drieat.ile-de-france.developpement-durable.gouv.fr/){target="_blank"} : Direction Régionale et interdépartementale de l’environnement de l’aménagement et des transports

[INSEE](https://www.insee.fr/fr/statistiques/2654964){target="_blank"} :  Institut national de la statistique et des études économiques

[CGDD/SDES](https://www.statistiques.developpement-durable.gouv.fr/){target="_blank"} : Données et études statistiques MTES

[ORS](https://www.ors-idf.org/){target="_blank"} : Observatoire Régional de Santé 

[IPR](https://www.institutparisregion.fr/){target="_blank"} : Institut Paris Région

[TEDDIF](https://www.teddif.org/){target="_blank"} :  Réseau francilien «Territoires, environnement et développement-durable en Ile-de-France»

[APUR](https://www.apur.org/fr){target="_blank"} : Atelier Parisien d’Urbanisme

[AMIF](https://amif.asso.fr/){target="_blank"} : association des maires d’île de France

[CEREMA](https://www.cerema.fr/fr/cerema/directions/cerema-ile-france){target="_blank"} : Centre d'études et d'expertise sur les risques, l'environnement, la mobilité et l'aménagement

Collectivités : Villeneuve-la-Garenne, Ville de Paris

Association : [Notre Village](http://www.notrevillage.fr/){target="_blank"}



## Données utilisées

La géographie utilisée pour les indicateurs communaux est celle du 1er janvier 2023.



##DD
