## Objectifs de l'Observatoire 

Créé par la [DRIEAT](https://www.drieat.ile-de-france.developpement-durable.gouv.fr/), l'Observatoire francilien des [Objectifs de Développement Durable (ODD)](https://www.un.org/sustainabledevelopment/fr/objectifs-de-developpement-durable/) est une application destinée à  donner à tous les acteurs territoriaux (collectivités, services de l’État, mais aussi citoyens) une vision synthétique de leur territoire au regard des 17 ODD sur la base de données et d'indicateurs pertinents et fiables. 

Outil à vocation opérationnelle, l'Observatoire propose :

* des cartographies avec des données disponibles aux échelles communales, EPCI (dont les EPT) et régionales ;
* des portraits de territoire qui permettent de disposer d'une vision synthétique de tous les indicateurs d'un territoire, de suivre leur évolution dans le temps et de comparer plusieurs territoires entre eux ;
* un catalogue de projets inspirants valorisant un ou plusieurs ODD

L'objectif est de faciliter la mise en œuvre de stratégies territoriales durables en s'appuyant sur des données objectives pour réaliser un état des lieux, favoriser l'appropriation et la déclinaison territoriale des ODD. 
