<style>
table {
  width: 100%;
  border-collapse: collapse;
}
th, td {
  border: 2px solid #ddd;
  padding: 8px;
}
</style>

| Données aux échelles internationale et européenne | Description synthétique            |
|------------------------------------------------|---------------------------------------|
| [Statistiques de l’ONU - ODD](https://unstats.un.org/UNSDWebsite/undatacommons/sdgs){target="_blank"} | Statistiques mondiales par ODD consultables à l’échelle internationale ou par pays |
| [Eurostat pour les ODD](https://ec.europa.eu/eurostat/cache/website/sdg/sdg_key/sdg_key_2023/index.html?lang=en){target="_blank"} | Visualisation synthétique du suivi des progrès par ODD à l’échelle européenne |  
  
| À l’échelle nationale | |
|------------------------------------------------|---------------------------------------|  
| [Agenda 2030- Rosace ODD](https://www.agenda-2030.fr/rosace/index2022.html?&ODD=2){target="_blank"} | Agenda 2030- Rosace ODD propose une datavisualisation des indicateurs de suivi des ODD à l’échelle nationale. Un outil pour connaître les tendances et la dynamique de la France en matière d’atteinte des ODD. |
| [ODDetT](https://oddett.lab.sspcloud.fr/app/dealapp){target="_blank"} | ODDetT est une initiative de la DEAL Réunion qui permet, pour tous les territoires métropolitains et ultramarins, de visualiser des représentations des indicateurs du développement durable. Vous pouvez accéder à ces résultats sur votre territoire et sur les territoires voisins, et vous comparer à d’autres niveaux géographiques.|
| [INSEE – indicateurs de suivi des ODD](https://www.insee.fr/fr/statistiques/2654964){target="_blank"} | Suivi des 98 indicateurs constituant le cadre national pour le suivi des progrès de la France dans l’atteinte des 17 ODD |
| [MTE- Outil EnR Etat des lieux](https://ssm-ecologie.shinyapps.io/outilEtatdesLieuxEnR/){target="_blank"} | Outil d’aide à la décision proposant des informations essentielles sur l’évolution des EnR territorialisées par EPCI. Précisant la production par filière, la puissance et les sites de production. |
| [Données et Transition](https://opendatafrance.gitbook.io/la-donnee-verte){target="_blank"} | Données et Transition vise à rechercher les meilleures conditions pour mettre la donnée au service de la transition environnementale (« data for green »). |
| [France Chaleur urbaine](https://france-chaleur-urbaine.beta.gouv.fr/){target="_blank"} | France Chaleur Urbaine est un service numérique de l’administration qui vise à faciliter et multiplier les raccordements aux réseaux de chaleur. |  

| Données à l’échelle régionale Île-de-France | |
|------------------------------------------------|---------------------------------------|
| [Observatoire francilien des ODD](https://ssm-ecologie.shinyapps.io/observatoire-odd/accueil){target="_blank"} | L’Observatoire francilien des Objectifs de Développement Durable propose des datavisualisations de votre commune ou EPCI au regard des 17 ODD ainsi qu’un catalogue de projets franciliens contribuant aux ODD. Panel d’indicateurs pertinents, cartographies dynamiques, graphiques comparatifs accessibles en ligne. |
| [BATISTATO](https://ssm-ecologie.shinyapps.io/batistato/){target="_blank"} | BATISTATO permet de visualiser la constitution des parcs de bâtiments résidentiels et tertiaires dans les territoires franciliens, mais également leurs consommations énergétiques associées. |
| [Cartoviz Institut Paris Région](https://www.institutparisregion.fr/cartographies-interactives-cartoviz/){target="_blank"} | Datavisualisations multi-thématiques de données franciliennes à des échelles très fines du territoire. |
| [Cartovégétation](https://experience.arcgis.com/experience/12f26d4d8f744a7e8f84c6e04d54df46/){target="_blank"} | Cartovégétation un outil de cartographie à l’échelle francilienne pour mieux prendre en compte la biodiversité : données sur la strate arborée et la strate herbacée à partir des bases IGN-F |
| [ROSE IdF](https://www.roseidf.org/){target="_blank"} | L’objectif du ROSE est de rassembler, consolider, traiter et diffuser les informations, les données et les scénarios relatifs à la consommation et à la production d’énergie, ainsi qu’aux émissions de gaz à effet de serre associées. |  

| Autres observatoires régionaux | |
|------------------------------------------------|---------------------------------------|
| [Observatoire Normand des Transitions](https://ssm-ecologie.shinyapps.io/observatoire-normand-des-transitions/les-transitions){target="_blank"} | L’Observatoire normand des transitions est une application destinée à donner aux acteurs du territoire (associations, citoyens, collectivités, professionnels, et services de l’État) une vision synthétique de la situation des établissements publics de coopération intercommunale (EPCI) normands au regard des transitions à mettre en œuvre sur la base d’indicateurs pertinents et fiables. |
| [ODDyssée normande – carte interactive](ODDyssée normande – carte interactive){target="_blank"}|Carte interactive  pour valoriser les Actions Normandes mises en œuvre pour répondre aux ODD. Visuels disponibles à plusieurs échelles de territoires et répartis par ODD. |
| [Cartographie Statistique Nouvelle Aquitaine : SIGENA](https://www.sigena.fr/geoclip/#c=indicator){target="_blank"} | Outil d’aide à la décision proposant des indicateurs structurés par thèmes et enjeux, dont les 17 Objectifs de développement durable, à l’échelle du territoire de la Nouvelle Aquitaine. Des portraits de territoires sont également proposés sous forme de graphiques. |
| [Cartopass Provence-Alpes Côte d’Azur](https://www.paca.developpement-durable.gouv.fr/cartopas-actuel-a14590.html){target="_blank"} | Atlas cartographique régional traduisant, à l’appui de 71 cartes au format pdf, les principales thématiques de l‘environnement, de l‘aménagement et du logement en région Provence-Alpes-Côte d’Azur. Les cartes sont mises à jour au fur et à mesure de l’évolution des données. |
| [Picto Stat - Occitanie](https://www.picto-occitanie.fr/geoclip/#c=home){target="_blank"} | Visualisation de données statistiques de l’ETat sur les territoires d’Occitanie, produites dans le cadre de partenariats inter-institutionnels.  Possibilité de cartographier les indicateurs, de visualiser et comparer des zonages, d’éditer des rapports. |
| [Cartographie des ODD – Pays de la Loire](https://carto.sigloire.fr/1/layers/fc262574-52a5-4945-8476-574fb4f020e2.map){target="_blank"} | Des fiches par EPCI ont été produites pour mettre en évidence pour ces territoires les principaux enjeux et leviers d’amélioration pour progresser vers les objectifs de développement durable (ODD) de l’agenda 2030 au regard du niveau régional.Elles caractérisent l’EPCI au travers d’un nombre restreints d’indicateurs économiques, sociaux et environnementaux. |




##DD


