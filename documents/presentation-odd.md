## Les objectifs de développement durable (ODD)

#### **Vers une déclinaison francilienne**.

En septembre 2015, les 193 États membres de l’ONU ont adopté le programme de développement durable à l’horizon 2030, intitulé Agenda 2030 pour lequel 17 Objectifs de développement durable (ODD) et 169 cibles ont été fixés.  

Ces objectifs dessinent des feuilles de routes et constituent un plan d’action pour la paix, l’humanité, la planète et la prospérité, nécessitant la mise en œuvre de partenariats multi-acteurs. Ils ambitionnent de transformer nos sociétés en éradiquant la pauvreté et en assurant une transition juste vers un développement durable d'ici à 2030.  

Les 17 objectifs développement durable (ODD) doivent être atteints collectivement et mis en œuvre. C’est pourquoi des dispositifs de suivi avec des indicateurs sont déployés au niveau international, mais également aux échelles nationales et territoriales.  

Pour connaître les principaux enjeux en île de France par ODD et découvrir une sélection d’indicateurs de suivi, cliquez sur un ODD :
