
L'Observatoire francilien des objectifs de développement durable (ODD) vous propose un catalogue de projets inspirants qui répondent de manière ciblée à un ou plusieurs ODD. Sous forme de fiches synthétiques, le catalogue offre une vision synthétique des projets, des contacts ainsi que des ressources complémentaires. En illustrant chacun des ODD par des projets concrets, il s'agit de faciliter leur appropriation mais également de valoriser les bonnes pratiques pour mieux les diffuser et les déployer.

Encore à son stade initial, ce catalogue a vocation à s'enrichir  pour chacun des ODD grâce aux partenariats en cours (notamment avec le Teddif) et à venir avec l'ensemble des acteurs de la transition écologique du territoire francilien.

Faites votre choix en précisant un ODD ou complétez la recherche textuelle avec des mots clés. Vous pouvez télécharger sous format PDF les fiches projet.
