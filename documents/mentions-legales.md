## Mentions légales

### Service gestionnaire

Ministère de la Transition écologique et Ministère de la Cohésion des territoires et des Relations avec les collectivités territoriales.
Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports (DRIEAT) Île-de-France - 21-23 rue Miollis, 75732 Paris cedex 15

### Directrice de la publication

Emmanuelle GAY, directrice régionale de la Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports (DRIEAT) Île-de-France

### Hébergement

* Rstudio - plateforme Shinyapps http://shinyapps.io/ 
* Le site est hébergé par le Ministère de la Transition écologique (secrétariat général/service du numérique/sous-direction des méthodes et services de plateforme/ département infrastructures et services) dont les serveurs se situent 4 Impasse Pierre Raymond, 33160 Saint-Médard-en-Jalles. SIREN : 110068012
* Siège social : Ministère de la Transition écologique hôtel Roquelaure, 246 Boulevard Saint-Germain, 75007 PARIS
    
### Droit d’auteur - Licence

Tous les contenus présents sur le site de la Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports sont couverts par le droit d’auteur. Toute reprise est dès lors conditionnée à l’accord de l’auteur en vertu de l’article L.122-4 du Code de la Propriété Intellectuelle. 
Toutes les informations liées à cette application (données et textes) sont publiées sous [licence ouverte/open licence v2] (https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf) (dite licence Etalab) : quiconque est libre de réutiliser ces informations, sous réserve notamment, d'en mentionner la filiation. 
Tous les scripts source de l'application sont disponibles sous [licence GPL-v3] (https://spdx.org/licenses/GPL-3.0.html#licenseText).

### Usage

* Les utilisateurs sont responsables des interrogations qu’ils formulent ainsi que de l’interprétation et de l’utilisation qu’ils font des résultats. Il leur appartient d’en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l’informatique, aux fichiers et aux libertés dite loi informatique et libertés). 
* Il appartient à l’utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d’éventuels virus circulant sur le réseau Internet. De manière générale, la Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports décline toute responsabilité à un éventuel dommage survenu pendant la consultation du présent site.

### Evolution du service

La Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports ajoute régulièrement des données, modifie l’interface les formulations sur la base des retours des usagers et des évolutions réglementaires et législatives. Elle se réserve le droit de faire évoluer le site sans information préalable ou préavis. 

### Disponibilité du service

L’accès au site peut être suspendu  sans information préalable ni préavis, notamment pour des raisons de maintenance. La Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports se réserve également le droit de bloquer, sans information préalable ni compensation financière, les usages mettant en péril l’utilisation du logiciel par d’autres usagers. 

### Traitement des données à caractère personnel

Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports s’engage à ne pas collecter des données à caractère personnel que l’internaute lui communique. Celles-ci sont confidentielles et ne seront utilisées que pour les besoins du service.

### Conception

Ce site a été conçu et développé par [dreamRs] (https://www.dreamrs.fr/) pour la Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports (DRIEAT).

### Code source

Le code source de l'application est mis à disposition sur le [Gitlab du SCDD] (https://gitlab.com/SCDD/DDGS/observatoire-francilien-des-odd)
