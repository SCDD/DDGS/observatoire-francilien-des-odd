# Version 2.5.6 du 03/03/2025

## Ajout de nouvelles fiches projets inspirants
**La Lanterne**  
**La Biosphère Urbaine**  

## Mise à jour de données
### ODD2 "Faim Zéro"
**Part des surfaces agricoles biologique ou en conversion :**
ajout des données 2021 et 2022

