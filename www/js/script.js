/*jshint
  browser:true,
  devel: true
*/
/*global LeafletWidget */

(function() {
   $(document).on("click", '[data-toggle="popover"]', function() {
     $(this).popover({sanitize: false, html: true});
     $(this).popover("show");
   });
  LeafletWidget.methods.addCustomContent = function(options) {
    (function(){
      var map = this;
      if (options.id) {
        var id = options.id;
        if (map[id]) {
          map[id].remove();
          delete map[id];
        }
        map[id] = L.control.custom(options);
        map.controls.add(map[id]);
      } else {
        map.customContent = L.control.custom(options);
        map.controls.add(map.customContent);
      }
    }).call(this);
  };
  LeafletWidget.methods.addZoom = function(options) {
    (function(){
      this.zoomControl.remove();
      L.control.zoom({ position: options.position }).addTo(this);
    }).call(this);
  };
  LeafletWidget.methods.addEasyPrint = function(options) {
    (function(){
      var map = this;
      var printcarto = L.easyPrint(options).addTo(map);
      Shiny.addCustomMessageHandler("telecharger-carto", function(obj) {
        printcarto.printMap("A4Landscape page", obj.fichier);
        Shiny.setInputValue("carto-spinner_show", true, {priority: "event"});
        map.on("easyPrint-finished", function (ev) {
          Shiny.setInputValue("carto-spinner_hide", true, {priority: "event"});
        }, 10000);
      });
    }).call(this);
  };
})();
